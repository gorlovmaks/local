<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AuthType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class IndexController extends Controller
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    /**
     * @Route("/", name="ping")
     * @return Response
     */
    public function indexAction()
    {
       return $this->render('index.html.twig');
    }

    /**
     * @Route("/register", name="register")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function registerAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getUsername(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword()
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute('ping');
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage();
            }
        }

        return $this->render('register.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }


    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        ApiContext $apiContext
    )
    {


        $user = new  User();
        $form = $this->createForm(AuthType::class,$user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


                if ($apiContext->clientExists($user->getPassword(),$user->getUsername())) {
                    $password = md5($user->getPassword()).md5($user->getPassword().'2');
                    $email = $user->getEmail();
                    $client = $userRepository->findOneByPassportAndEmail($password,$email);

                    if ($client){
                        $token = new UsernamePasswordToken($client, null, 'main', $user->getRoles());
                        $this
                            ->container
                            ->get('security.token_storage')
                            ->setToken($token);
                        $this
                            ->container
                            ->get('session')
                            ->set('_security_main', serialize($token));
                        return $this->redirectToRoute('ping');
                }

                }else {
                    return new Response('Проблемка');
                }

        }
        return $this->render('authentication.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
